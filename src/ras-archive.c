/* Copyright (C) 2017 Ernestas Kulik <ernestas DOT kulik AT gmail DOT com>
 *
 * This file is part of libras.
 *
 * libras is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libras is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libras.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "ras-archive.h"
#include "ras-decompressor.h"
#include "ras-decryptor.h"
#include "ras-directory.h"
#include "ras-file.h"
#include "ras-utils.h"

#include <string.h>
#include <zlib.h>

#define RAS_HEADER_LENGTH 0x2C
#define RAS_MAGIC "RAS"
#define RAS_MAGIC_LENGTH (strlen (RAS_MAGIC) + 1)
#define RAS_VERSION 3

enum
{
    RAS_HEADER_OFFSET_MAGIC = 0x0,
    RAS_HEADER_OFFSET_ENCRYPTION_SEED = 0x4,
    RAS_HEADER_OFFSET_FILE_COUNT = 0x8,
    RAS_HEADER_OFFSET_DIRECTORY_COUNT = 0xC,
    RAS_HEADER_OFFSET_FILE_TABLE_SIZE = 0x10,
    RAS_HEADER_OFFSET_DIRECTORY_TABLE_SIZE = 0x14,
    RAS_HEADER_OFFSET_ARCHIVER_VERSION = 0x18,
    RAS_HEADER_OFFSET_HEADER_CHECKSUM = 0x1C,
    RAS_HEADER_OFFSET_FILE_TABLE_CHECKSUM = 0x20,
    RAS_HEADER_OFFSET_DIRECTORY_TABLE_CHECKSUM = 0x24,
    RAS_HEADER_OFFSET_VERSION = 0x28,
};

enum
{
    RAS_DIRECTORY_HEADER_OFFSET_CREATION_TIME = 0x0,
    RAS_DIRECTORY_HEADER_OFFSET_END = 0x10,
};

enum
{
    RAS_FILE_HEADER_OFFSET_SIZE = 0x0,
    RAS_FILE_HEADER_OFFSET_COMPRESSED_SIZE = 0x4,
    //RAS_FILE_HEADER_OFFSET_??? = 0x8,
    RAS_FILE_HEADER_OFFSET_PARENT_DIRECTORY = 0xC,
    RAS_FILE_HEADER_OFFSET_ENCRYPTION_SEED = 0x10,
    RAS_FILE_HEADER_OFFSET_COMPRESSION_METHOD = 0x14,
    RAS_FILE_HEADER_OFFSET_CREATION_TIME = 0x18,

    RAS_FILE_HEADER_OFFSET_END = 0x28,
};

struct _RasArchive
{
    GObject parent_instance;

    GInputStream *stream;
    GList *file_table;
    GHashTable *directory_table;
    int64_t data_offset;
};

G_DEFINE_TYPE (RasArchive, ras_archive, G_TYPE_OBJECT)

static int32_t
read_int32le (const void *buffer)
{
    return GINT32_FROM_LE (*(const int32_t *) buffer);
}

static int32_t
read_uint32le (const void *buffer)
{
    return GUINT32_FROM_LE (*(const uint32_t *) buffer);
}

static void
ras_archive_finalize (GObject *object)
{
    RasArchive *self;

    self = RAS_ARCHIVE (object);

    g_clear_list (&self->file_table, g_object_unref);
    g_clear_pointer (&self->directory_table, g_hash_table_destroy);
    g_clear_object (&self->stream);

    G_OBJECT_CLASS (ras_archive_parent_class)->finalize (object);
}

static void
ras_archive_class_init (RasArchiveClass *klass)
{
    GObjectClass *object_class;

    object_class = G_OBJECT_CLASS (klass);

    object_class->finalize = ras_archive_finalize;
}

static void
ras_archive_init (RasArchive *self)
{
    self->stream = NULL;
    self->file_table = NULL;
    self->directory_table = g_hash_table_new_full (g_direct_hash, g_direct_equal,
                                                   NULL, g_object_unref);
}

GQuark
ras_archive_error_quark (void)
{
    return g_quark_from_static_string ("ras-archive-error-quark");
}

static RasDirectory *
ras_archive_get_directory_by_index (RasArchive   *self,
                                    unsigned int  index)
{
    const void *key;
    void *value;

    g_return_val_if_fail (RAS_IS_ARCHIVE (self), NULL);

    key = GUINT_TO_POINTER (index);
    value = g_hash_table_lookup (self->directory_table, key);

    return RAS_DIRECTORY (value);
}

GList *
ras_archive_get_files (RasArchive *self)
{
    g_return_val_if_fail (RAS_IS_ARCHIVE (self), NULL);
    g_return_val_if_fail (NULL != self->file_table, NULL);

    return g_list_copy (self->file_table);
}

bool
ras_archive_get_file_offset (RasArchive *self,
                             RasFile    *file,
                             ptrdiff_t  *offset)
{
    *offset = 0;

    for (GList *f = self->file_table; NULL != f; f = f->next)
    {
        if (f->data == file)
        {
            return true;
        }

        if (ras_file_is_compressed (RAS_FILE (f->data)))
        {
            *offset += ras_file_get_compressed_size (RAS_FILE (f->data));
        }
        else
        {
            *offset += ras_file_get_size (RAS_FILE (f->data));
        }
    }

    return false;
}

GBytes *
ras_archive_get_file_data (RasArchive    *self,
                           RasFile       *file,
                           GCancellable  *cancellable,
                           GError       **error)
{
    ptrdiff_t offset;
    g_autoptr (GInputStream) stream = NULL;
    size_t size;

    g_return_val_if_fail (RAS_IS_ARCHIVE (self), NULL);
    g_return_val_if_fail (RAS_IS_FILE (file), NULL);
    g_return_val_if_fail (G_IS_INPUT_STREAM (self->stream), NULL);
    g_return_val_if_fail (G_IS_SEEKABLE (self->stream), NULL);

    offset = 0;
    stream = g_object_ref (self->stream);
    size = ras_file_get_size (file);

    if (!ras_archive_get_file_offset (self, file, &offset))
    {
        g_set_error_literal (error,
                                G_IO_ERROR, G_IO_ERROR_NOT_FOUND,
                                "File not found in archive");

        return NULL;
    }

    if (ras_file_is_compressed (file))
    {
        offset += 12;
    }

    if (!g_seekable_seek (G_SEEKABLE (self->stream), self->data_offset + offset,
                          G_SEEK_SET, cancellable, error))
    {
        return NULL;
    }

    if (ras_file_is_compressed (file))
    {
        g_autoptr (RasDecompressor) decompressor = NULL;

        decompressor = ras_decompressor_new ();

        if (ras_file_is_encrypted (file))
        {
            int32_t seed;
            g_autoptr (RasDecryptor) decryptor = NULL;

            seed = ras_file_get_encryption_seed (file);
            decryptor = ras_decryptor_new (seed);

            stream = g_converter_input_stream_new (stream, G_CONVERTER (decryptor));
        }

        stream = g_converter_input_stream_new (stream, G_CONVERTER (decompressor));

        /* The base file stream should remain open, as it is reused. */
        g_filter_input_stream_set_close_base_stream (G_FILTER_INPUT_STREAM (stream),
                                                     false);
    }

    return g_input_stream_read_bytes (stream, size, cancellable, error);
}

static bool
populate_file_table (RasArchive     *archive,
                     const uint8_t  *data,
                     size_t          file_count,
                     GError        **error)
{
    g_assert (RAS_IS_ARCHIVE (archive));
    g_assert (NULL != data);

    for (size_t i = 0; i < file_count; i++)
    {
        const char *name;
        uint32_t size;
        uint32_t compressed_size;
        uint32_t parent_directory_index;
        int32_t encryption_seed;
        uint32_t compression_method;
        RasFile *file;
        RasDirectory *directory;

        name = (const char *) data;

        data += strlen (name) + 1;

        size = read_uint32le (data + RAS_FILE_HEADER_OFFSET_SIZE);
        compressed_size = read_uint32le (data + RAS_FILE_HEADER_OFFSET_COMPRESSED_SIZE);
        parent_directory_index = read_uint32le (data + RAS_FILE_HEADER_OFFSET_PARENT_DIRECTORY);
        encryption_seed = read_int32le (data + RAS_FILE_HEADER_OFFSET_ENCRYPTION_SEED);
        compression_method = read_uint32le (data + RAS_FILE_HEADER_OFFSET_COMPRESSION_METHOD);

        data += RAS_FILE_HEADER_OFFSET_END;

        file = ras_file_new ();
        directory = ras_archive_get_directory_by_index (archive, parent_directory_index);

        archive->file_table = g_list_prepend (archive->file_table, file);

        ras_file_set_compressed (file,
                                 (compression_method == RAS_FILE_COMPRESSION_METHOD_COMPRESS ||
                                  compression_method == RAS_FILE_COMPRESSION_METHOD_ENCRYPT));
        ras_file_set_compressed_size (file, compressed_size);
        ras_file_set_encrypted (file,
                                compression_method == RAS_FILE_COMPRESSION_METHOD_ENCRYPT);
        ras_file_set_encryption_seed (file, encryption_seed);
        ras_file_set_parent (file, directory);
        ras_file_set_name (file, name);
        ras_file_set_size (file, size);

        ras_directory_add_file (directory, file);
    }

    archive->file_table = g_list_reverse (archive->file_table);

    return true;
}

static void
insert_directory (RasArchive   *archive,
                  RasDirectory *directory)
{
    uint32_t table_size;
    void *key;

    g_assert (RAS_IS_ARCHIVE (archive));
    g_assert (RAS_IS_DIRECTORY (directory));

    table_size = g_hash_table_size (archive->directory_table);
    key = GUINT_TO_POINTER (table_size);

    g_assert (g_hash_table_insert (archive->directory_table, key, directory));
}

static bool
populate_directory_table (RasArchive     *archive,
                          const uint8_t  *data,
                          size_t          directory_count,
                          GError        **error)
{
    g_assert (RAS_IS_ARCHIVE (archive));

    for (size_t i = 0; i < directory_count; i++)
    {
        const char *name;
        RasDirectory *directory = NULL;

        name = (const char *) data;
        data += strlen (name) + 1 + RAS_DIRECTORY_HEADER_OFFSET_END;
        directory = ras_directory_new (name);

        g_debug ("Inserting directory to table: %s", name);

        insert_directory (archive, directory);
    }

    return true;
}

RasArchive *
ras_archive_new_from_stream (GInputStream  *stream,
                             GCancellable  *cancellable,
                             GError       **error)
{
    g_autoptr (RasArchive) archive = NULL;
    g_autoptr (GDataInputStream) data_stream = NULL;
    g_autoptr (GError) local_error = NULL;
    uint8_t header[RAS_HEADER_LENGTH] = { 0 };
    int32_t encryption_seed;
    g_autoptr (RasDecryptor) decryptor = NULL;
    size_t file_count;
    size_t directory_count;
    size_t file_table_size;
    size_t directory_table_size;

    g_return_val_if_fail (G_IS_INPUT_STREAM (stream), NULL);
    g_return_val_if_fail (G_IS_SEEKABLE (stream), NULL);

    archive = g_object_new (RAS_TYPE_ARCHIVE, NULL);

    archive->stream = g_object_ref (stream);

    if (g_input_stream_read (stream, header, sizeof (header), cancellable, &local_error) != sizeof (header))
    {
        _g_propagate_error_if_not_null (error, local_error);
        g_set_error_literal (error, RAS_ARCHIVE_ERROR, RAS_ERROR_TRUNCATED,
                             "Truncated file");

        return NULL;
    }

    if (memcmp (header, RAS_MAGIC, RAS_MAGIC_LENGTH) != 0)
    {
        g_set_error_literal (error, RAS_ARCHIVE_ERROR, RAS_ERROR_INVALID_MAGIC,
                            "Not a RAS archive");

        return NULL;
    }

    encryption_seed = read_int32le (header + RAS_HEADER_OFFSET_ENCRYPTION_SEED);
    decryptor = ras_decryptor_new (encryption_seed);

    {
        size_t dummy;

        if (g_converter_convert (G_CONVERTER (decryptor),
                                header + 8, 36,
                                header + 8, 36,
                                G_CONVERTER_INPUT_AT_END,
                                &dummy, &dummy,
                                error) == G_CONVERTER_ERROR)
        {
            return NULL;
        }
    }

    if (read_uint32le (header + RAS_HEADER_OFFSET_VERSION) < RAS_VERSION)
    {
        g_set_error_literal (error,
                            RAS_ARCHIVE_ERROR,
                            RAS_ERROR_UNSUPPORTED_VERSION,
                            "Unsupported archive version");

        return NULL;
    }

    {
        {
            uint32_t checksum;
            uint32_t crc;

            checksum = read_uint32le (header + RAS_HEADER_OFFSET_HEADER_CHECKSUM);

            memset (header + RAS_HEADER_OFFSET_HEADER_CHECKSUM, 0, sizeof (uint32_t));

            crc = crc32_z (0, Z_NULL, 0);
            crc = crc32_z (crc, header, RAS_HEADER_LENGTH);
            if (crc != checksum)
            {
                g_set_error_literal (error,
                                    RAS_ARCHIVE_ERROR,
                                    RAS_ERROR_UNSUPPORTED_VERSION,
                                    "Invalid header checksum");

                return NULL;
            }
        }

        file_count = read_uint32le (header + RAS_HEADER_OFFSET_FILE_COUNT);
        directory_count = read_uint32le (header + RAS_HEADER_OFFSET_DIRECTORY_COUNT);
        file_table_size = read_uint32le (header + RAS_HEADER_OFFSET_FILE_TABLE_SIZE);
        directory_table_size = read_uint32le (header + RAS_HEADER_OFFSET_DIRECTORY_TABLE_SIZE);
    }

    archive->data_offset = RAS_HEADER_LENGTH + file_table_size + directory_table_size;

    {
        g_autoptr (GError) local_error = NULL;
        g_autoptr (GBytes) bytes = NULL;
        const uint8_t *data = NULL;
        g_autofree uint8_t *directory_table = NULL;
        uint32_t checksum;
        uint32_t crc;

        if (g_input_stream_skip (stream, file_table_size, cancellable, &local_error) < file_table_size)
        {
            _g_propagate_error_if_not_null (error, local_error);
            g_set_error_literal (error,
                                    RAS_ARCHIVE_ERROR,
                                    RAS_ERROR_TRUNCATED,
                                    "Truncated file");

            return NULL;
        }

        bytes = g_input_stream_read_bytes (stream, directory_table_size,
                                           cancellable, error);
        if (NULL == bytes)
        {
            return NULL;
        }

        if (g_bytes_get_size (bytes) < directory_table_size)
        {
            g_set_error_literal (error,
                                 RAS_ARCHIVE_ERROR,
                                 RAS_ERROR_TRUNCATED,
                                 "Truncated file");

            return NULL;
        }

        data = g_bytes_get_data (bytes, NULL);
        directory_table = malloc (directory_table_size);
        checksum = read_uint32le (header + RAS_HEADER_OFFSET_DIRECTORY_TABLE_CHECKSUM);

        {
            size_t dummy;

            g_converter_reset (G_CONVERTER (decryptor));
            g_converter_convert (G_CONVERTER (decryptor),
                                data, directory_table_size,
                                directory_table, directory_table_size,
                                G_CONVERTER_INPUT_AT_END,
                                &dummy, &dummy,
                                NULL);
        }

        crc = crc32_z (0, Z_NULL, 0);
        crc = crc32_z (crc, directory_table, directory_table_size);
        if (crc != checksum)
        {
            g_set_error_literal (error,
                                 RAS_ARCHIVE_ERROR,
                                 RAS_ERROR_INVALID_CHECKSUM,
                                 "Invalid directory table checksum");

            return NULL;
        }

        if (!populate_directory_table (archive, directory_table, directory_count, error))
        {
            return NULL;
        }
    }

    if (!g_seekable_seek (G_SEEKABLE (stream), RAS_HEADER_LENGTH, G_SEEK_SET,
                          cancellable, error))
    {
        return NULL;
    }

    {
        g_autoptr (GBytes) bytes = NULL;
        const uint8_t *data;
        g_autofree uint8_t *file_table = NULL;
        uint32_t checksum;
        uint32_t crc;

        bytes = g_input_stream_read_bytes (stream, file_table_size,
                                           cancellable, error);
        if (NULL == bytes)
        {
            return NULL;
        }

        if (g_bytes_get_size (bytes) < file_table_size)
        {
            g_set_error_literal (error,
                                 RAS_ARCHIVE_ERROR,
                                 RAS_ERROR_TRUNCATED,
                                 "Truncated file");

            return NULL;
        }

        data = g_bytes_get_data (bytes, NULL);
        file_table = malloc (file_table_size);
        checksum = read_uint32le (header + RAS_HEADER_OFFSET_FILE_TABLE_CHECKSUM);

        {
            size_t dummy;

            g_converter_reset (G_CONVERTER (decryptor));
            g_converter_convert (G_CONVERTER (decryptor),
                                data, file_table_size,
                                file_table, file_table_size,
                                G_CONVERTER_INPUT_AT_END,
                                &dummy, &dummy,
                                NULL);
        }

        crc = crc32_z (0, Z_NULL, 0);
        crc = crc32_z (crc, file_table, file_table_size);
        if (crc != checksum)
        {
            g_set_error_literal (error,
                                 RAS_ARCHIVE_ERROR,
                                 RAS_ERROR_INVALID_CHECKSUM,
                                 "Invalid file table checksum");

            return NULL;
        }

        if (!populate_file_table (archive, file_table, file_count, error))
        {
            return NULL;
        }
    }

    return g_steal_pointer (&archive);
}
