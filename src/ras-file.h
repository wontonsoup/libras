/* Copyright (C) 2018 Ernestas Kulik <ernestas DOT kulik AT gmail DOT com>
 *
 * This file is part of libras.
 *
 * libras is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libras is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libras.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "ras-types.h"

#include <gio/gio.h>
#include <glib-object.h>
#include <stdbool.h>
#include <stdint.h>

#define CMPHEADER "RA->"
#define CRYPTHEADER "RC->"

G_BEGIN_DECLS

G_DECLARE_FINAL_TYPE (RasFile, ras_file, RAS, FILE, GObject)

typedef enum
{
    RAS_FILE_COMPRESSION_METHOD_INVALID = -1,
    RAS_FILE_COMPRESSION_METHOD_COMPRESS = 1,
    RAS_FILE_COMPRESSION_METHOD_ENCRYPT = 2,
    RAS_FILE_COMPRESSION_METHOD_STORE = 3
} RasCompressionMethod;

size_t        ras_file_get_compressed_size    (RasFile *file);
int32_t       ras_file_get_encryption_seed    (RasFile *file);
char         *ras_file_get_name               (RasFile *file);
RasDirectory *ras_file_get_parent             (RasFile *file);
size_t        ras_file_get_size               (RasFile *file);

bool          ras_file_is_compressed (RasFile *file);
bool          ras_file_is_encrypted (RasFile *file);

void          ras_file_set_compressed (RasFile *file,
                                       bool     compressed);
void          ras_file_set_compressed_size (RasFile *file,
                                            size_t   size);
void          ras_file_set_encrypted (RasFile *file,
                                      bool     compressed);
void          ras_file_set_encryption_seed (RasFile *file,
                                            int32_t seed);
void          ras_file_set_data_bytes (RasFile *file,
                                       GBytes  *bytes);
void          ras_file_set_parent    (RasFile      *file,
                                      RasDirectory *directory);
void          ras_file_set_name      (RasFile    *file,
                                      const char *name);
void          ras_file_set_size      (RasFile *file,
                                      size_t   size);
void          ras_file_set_stream    (RasFile *file,
                                      GInputStream *stream);

RasFile *ras_file_new (void);
