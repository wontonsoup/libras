/* Copyright (C) 2023 Ernestas Kulik <ernestas AT baltic DOT engineering>
 *
 * This file is part of libras.
 *
 * libras is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libras is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libras.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "ras-file.h"
#include "ras-decompressor.h"

#define BUFFER_SIZE 4096
#define F 18
#define THRESHOLD 3

#define ras_decompressor_push_literal(self, output, literal) \
    do \
    { \
        self->buffer[self->cursor] = literal; \
        self->cursor = (self->cursor + 1) % BUFFER_SIZE; \
        *output = literal; \
    } while (false);

struct _RasDecompressor
{
    GObject parent_instance;

    uint8_t *buffer;
    size_t cursor;
};

static GConverterResult
ras_decompressor_convert (GConverter       *converter,
                          const void       *inbuf,
                          gsize             inbuf_size,
                          void             *outbuf,
                          gsize             outbuf_size,
                          GConverterFlags   flags,
                          gsize            *bytes_read,
                          gsize            *bytes_written,
                          GError          **error)
{
    RasDecompressor *self;
    const uint8_t *indata;
    size_t written;

    self = RAS_DECOMPRESSOR (converter);
    indata = inbuf;
    written = 0;

    if (flags & G_CONVERTER_FLUSH)
    {
        return G_CONVERTER_FLUSHED;
    }

    /* With the current threshold (3) and dictionary size (4096), the best
     * compression ratio that can be achieved is ~0.118, so let’s request a
     * bigger output buffer.
     */
    if (outbuf_size <= inbuf_size * 8)
    {
        g_set_error_literal (error, G_IO_ERROR, G_IO_ERROR_NO_SPACE,
                             "Output buffer not big enough");

        return G_CONVERTER_ERROR;
    }

    for (size_t rpos = 0; rpos < inbuf_size; )
    {
        uint8_t flags;

        flags = indata[rpos];
        rpos += 1;

        for (int i = 0; i < 8 && rpos < inbuf_size; i++)
        {
            if (flags & (1 << i))
            {
                ras_decompressor_push_literal (self, (uint8_t *) outbuf, indata[rpos]);

                outbuf += 1;
                written += 1;
                rpos += 1;
            }
            else
            {
                uint16_t pointer;
                uint8_t length;

                pointer = (indata[rpos + 1] & 0xF0) << 4  | indata[rpos];
                length = (indata[rpos + 1] & 0xF) + 3;

                for (uint8_t i = 0; i < length; i++)
                {
                    ras_decompressor_push_literal (self, (uint8_t *) outbuf,
                                                   self->buffer[(pointer + F + i) % BUFFER_SIZE]);

                    outbuf += 1;
                    written += 1;
                }

                rpos += 2;
            }
        }
    }

    if (NULL != bytes_read)
    {
        *bytes_read = inbuf_size;
    }
    if (NULL != bytes_written)
    {
        *bytes_written = written;
    }

    if (flags & G_CONVERTER_INPUT_AT_END)
    {
        return G_CONVERTER_FINISHED;
    }

    return G_CONVERTER_CONVERTED;
}

static void
ras_decompressor_reset (GConverter *converter)
{
    RasDecompressor *self;

    self = RAS_DECOMPRESSOR (converter);

    self->cursor = 0;

    g_clear_pointer (&self->buffer, g_free);
}

static void
ras_decompressor_iface_init (GConverterIface *iface)
{
    iface->convert = ras_decompressor_convert;
    iface->reset = ras_decompressor_reset;
}

G_DEFINE_TYPE_WITH_CODE (RasDecompressor, ras_decompressor,
                         G_TYPE_OBJECT,
                         G_IMPLEMENT_INTERFACE (G_TYPE_CONVERTER,
                                                ras_decompressor_iface_init))

static void
ras_decompressor_class_init (RasDecompressorClass *klass)
{
}

static void
ras_decompressor_init (RasDecompressor *self)
{
    self->buffer = g_malloc0 (BUFFER_SIZE);
    self->cursor = 0;
}

RasDecompressor *
ras_decompressor_new (void)
{
    return g_object_new (RAS_TYPE_DECOMPRESSOR, NULL);
}
