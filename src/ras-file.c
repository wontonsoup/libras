/* Copyright (C) 2018 Ernestas Kulik <ernestas DOT kulik AT gmail DOT com>
 *
 * This file is part of libras.
 *
 * libras is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * libras is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with libras.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "ras-directory.h"
#include "ras-file.h"

#include <string.h>

struct _RasFile
{
    GObject parent_instance;

    GString *name;
    RasDirectory *parent;
    bool compressed;
    size_t compressed_size;
    bool encrypted;
    int32_t encryption_seed;
    size_t size;
};

G_DEFINE_TYPE (RasFile, ras_file, G_TYPE_OBJECT)

static void
finalize (GObject *object)
{
    RasFile *file;

    file = RAS_FILE (object);

    file->size = 0;
    file->encryption_seed = 0;
    file->encrypted = false;
    file->compressed_size = 0;
    file->compressed = false;
    g_clear_object (&file->parent);
    (void) g_string_free (file->name, true);

    G_OBJECT_CLASS (ras_file_parent_class)->finalize (object);
}

static void
ras_file_class_init (RasFileClass *klass)
{
    GObjectClass *object_class;

    object_class = G_OBJECT_CLASS (klass);

    object_class->finalize = finalize;
}

static void
ras_file_init (RasFile *self)
{
    self->name = g_string_new (NULL);
}

size_t
ras_file_get_compressed_size (RasFile *self)
{
    g_return_val_if_fail (RAS_IS_FILE (self), 0);

    return self->compressed_size;
}

int32_t
ras_file_get_encryption_seed (RasFile *self)
{
    g_return_val_if_fail (RAS_IS_FILE (self), 0);

    return self->encryption_seed;
}

char *
ras_file_get_name (RasFile *self)
{
    g_return_val_if_fail (RAS_IS_FILE (self), NULL);
    g_return_val_if_fail (NULL != self->name, NULL);

    return g_strdup (self->name->str);
}

RasDirectory *
ras_file_get_parent (RasFile *self)
{
    g_return_val_if_fail (RAS_IS_FILE (self), NULL);

    return g_object_ref (self->parent);
}

size_t
ras_file_get_size (RasFile *self)
{
    g_return_val_if_fail (RAS_IS_FILE (self), 0);

    return self->size;
}

bool
ras_file_is_compressed (RasFile *self)
{
    g_return_val_if_fail (RAS_IS_FILE (self), false);

    return self->compressed;
}

bool
ras_file_is_encrypted (RasFile *self)
{
    g_return_val_if_fail (RAS_IS_FILE (self), false);

    return self->encrypted;
}

void
ras_file_set_compressed (RasFile *self,
                         bool     compressed)
{
    g_return_if_fail (RAS_IS_FILE (self));

    self->compressed = compressed;
}

void
ras_file_set_compressed_size (RasFile *self,
                              size_t   size)
{
    g_return_if_fail (RAS_IS_FILE (self));

    self->compressed_size = size;
}

void
ras_file_set_encrypted (RasFile *self,
                        bool     encrypted)
{
    g_return_if_fail (RAS_IS_FILE (self));

    self->encrypted = encrypted;
}

void
ras_file_set_encryption_seed (RasFile *self,
                              int32_t  seed)
{
    g_return_if_fail (RAS_IS_FILE (self));

    self->encryption_seed = seed;
}

void
ras_file_set_parent (RasFile      *self,
                     RasDirectory *directory)
{
    g_return_if_fail (RAS_IS_FILE (self));
    g_return_if_fail (RAS_IS_DIRECTORY (directory));

    g_clear_object (&self->parent);

    self->parent = g_object_ref (directory);
}

void
ras_file_set_name (RasFile    *self,
                   const char *name)
{
    g_return_if_fail (RAS_IS_FILE (self));
    g_return_if_fail (NULL != name);

    self->name = g_string_assign (self->name, name);
}

void
ras_file_set_size (RasFile *self,
                   size_t   size)
{
    g_return_if_fail (RAS_IS_FILE (self));

    self->size = size;
}

RasFile *
ras_file_new (void)
{
    return g_object_new (RAS_TYPE_FILE, NULL);
}
