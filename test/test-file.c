#include <locale.h>
#include <stdlib.h>

#include <ras-archive.h>
#include <ras-directory.h>
#include <ras-file.h>

static bool
extract_file (RasArchive  *archive,
              RasFile     *file,
              bool         force,
              GFile       *location,
              GError     **error)
{
    g_autofree char *file_name = NULL;
    g_autoptr (GFile) out_file = NULL;
    g_autoptr (GFileOutputStream) stream = NULL;
    g_autoptr (GBytes) bytes = NULL;
    const void *data = NULL;
    size_t size;

    file_name = ras_file_get_name (file);
    out_file = g_file_get_child (location, file_name);
    if (force)
    {
        stream = g_file_replace (out_file, NULL, false,
                                 G_FILE_CREATE_REPLACE_DESTINATION, NULL,
                                 error);
    }
    else
    {
        stream = g_file_create (out_file, G_FILE_CREATE_NONE, NULL, error);
    }
    if (NULL == stream)
    {
        return false;
    }

    g_message ("Extracting %s…", file_name);

    bytes = ras_archive_get_file_data (archive, file, NULL, error);
    if (NULL == bytes)
    {
        return false;
    }
    data = g_bytes_get_data (bytes, NULL);
    size = ras_file_get_size (file);

    return g_output_stream_write_all (G_OUTPUT_STREAM (stream), data, size,
                                      NULL, NULL, error);
}

int
main (int    argc,
      char **argv)
{
    g_autoptr (GOptionContext) option_context = NULL;
    gboolean decompress = false;
    gboolean force = false;
    const char *only = NULL;
    const char *output_dir = "";
    g_auto (GStrv) paths = NULL;
    g_autoptr (GError) error = NULL;
    const GOptionEntry option_entries[] =
    {
        {
            "decompress", 0, G_OPTION_FLAG_NONE,
            G_OPTION_ARG_NONE, &decompress,
            "Decompress FILE", NULL,

        },
        {
            "force", 'f', G_OPTION_FLAG_NONE,
            G_OPTION_ARG_NONE, &force,
            "Overwrite existing files", NULL,
        },
        {
            "only", 0, G_OPTION_FLAG_NONE,
            G_OPTION_ARG_STRING, &only,
            "Only process ENTRY", "ENTRY",
        },
        {
            "output-dir", 'O', G_OPTION_FLAG_NONE,
            G_OPTION_ARG_STRING, &output_dir,
            "Output files to DIR", "DIR",
        },
        {
            G_OPTION_REMAINING, 0, G_OPTION_FLAG_NONE,
            G_OPTION_ARG_FILENAME_ARRAY, &paths,
            NULL, NULL,
        },
        {
            NULL, 0, 0,
            0, NULL,
            NULL, NULL,
        }
    };
    g_autoptr (GMappedFile) file = NULL;
    g_autoptr (GBytes) bytes = NULL;
    g_autoptr (GInputStream) stream = NULL;
    g_autoptr (RasArchive) archive = NULL;
    g_autoptr (GList) files = NULL;

    setlocale (LC_ALL, "");

    option_context = g_option_context_new ("[FILE]");

    g_option_context_add_main_entries (option_context, option_entries, NULL);

    if (!g_option_context_parse (option_context, &argc, &argv, &error))
    {
        g_printerr ("%s\n", error->message);

        return EXIT_FAILURE;
    }

    if (NULL == paths)
    {
        g_printerr ("No file specified\n");

        return EXIT_FAILURE;
    }

    file = g_mapped_file_new (paths[0], false, &error);
    if (NULL == file)
    {
        g_printerr ("Failed to open archive: %s\n", error->message);

        return EXIT_FAILURE;
    }
    bytes = g_mapped_file_get_bytes (file);
    stream = g_memory_input_stream_new_from_bytes (bytes);
    archive = ras_archive_new_from_stream (stream, NULL, &error);
    if (NULL == archive)
    {
        g_printerr ("Failed to load archive: %s\n", error->message);

        return EXIT_FAILURE;
    }
    files = ras_archive_get_files (archive);

    if (!decompress)
    {
        for (GList *f = files; NULL != f; f = f->next)
        {
            RasFile *file = NULL;
            g_autoptr (RasDirectory) directory = NULL;
            g_autofree char *dir_name = NULL;
            g_autofree char *file_name = NULL;

            file = RAS_FILE (f->data);
            directory = ras_file_get_parent (file);
            dir_name = ras_directory_get_name (directory, true);
            file_name = ras_file_get_name (file);

            g_print ("%s%s\n", dir_name, file_name);
        }

        return EXIT_SUCCESS;
    }

    if (decompress)
    {
        for (GList *f = files; NULL != f; f = f->next)
        {
            RasFile *file = NULL;
            g_autoptr (RasDirectory) dir = NULL;
            g_autofree char *dir_name = NULL;
            g_autofree char *file_name = NULL;

            file = RAS_FILE (f->data);
            dir = ras_file_get_parent (file);
            dir_name = ras_directory_get_name (dir, true);
            file_name = ras_file_get_name (file);

            if (NULL != only && g_strcmp0 (only, file_name) != 0)
            {
                g_debug ("Skipping %s…", file_name);

                continue;
            }

            if (!ras_directory_is_root (dir))
            {
                g_autofree char *location = NULL;
                g_autoptr (GFile) dir_file = NULL;

                location = g_build_path (G_DIR_SEPARATOR_S, output_dir, dir_name, NULL);
                dir_file = g_file_new_for_path (location);

                if (!g_file_make_directory_with_parents (dir_file, NULL, &error))
                {
                    if (!g_error_matches (error, G_IO_ERROR, G_IO_ERROR_EXISTS))
                    {
                        g_printerr ("Failed to create directory %s: %s\n",
                                    dir_name, error->message);
                    }

                    g_clear_error (&error);
                }

                if (!extract_file (archive, file, force, dir_file, &error))
                {
                    g_printerr ("Failed to extract %s: %s\n",
                                file_name, error? error->message : "");

                    return EXIT_FAILURE;
                }
            }
        }
    }

    return EXIT_SUCCESS;
}
